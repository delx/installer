#!/bin/bash

# based on this doc from docker
# https://docs.docker.com/engine/install/debian/

# make sure everything is up to date
apt update
apt -y dist-upgrade

# in case of
apt remove docker docker-engine docker.io containerd runc

# preparing installation
apt -y install ca-certificates curl gnupg lsb-release python3-pip
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list 

# installing docker
apt update
apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# installing docker-compose
pip3 install docker-compose

# testing docker
docker run hello-world

