#!/bin/bash

apt update
apt -y full-upgrade

apt -y install gnupg2
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
apt -y install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt  update && apt -y install elasticsearch
apt clean

echo "network.host: 127.0.0.1" >> /etc/elasticsearch/elasticsearch.yml

/bin/systemctl daemon-reload
/bin/systemctl enable elasticsearch.service
/bin/systemctl start elasticsearch.service
